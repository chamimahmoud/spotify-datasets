This project is made to perform exploratory data analysis using Pyton on music
related datasets. Then using a tool for visualization Power Bi.
Spotify is the world's largest audio streaming platform.
The step we will follow in this project are:
 - Import our data 
 - Check and Analyze the Data
 - Figure out the correlation
 - Visualize the data with Python librairies
 - Use PowerBi for more visualization

Tools Used:

Python, Pandas, Seaborn, Matplotlib
PowerBi
Jupyter, Gitlab
